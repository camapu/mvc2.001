package net.canos.spring.webapp;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/mapping")
public class RequestMappingController {

	@ResponseBody
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public String get(){
		return "get";
	}
	
	@ResponseBody
	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String post(){		
		return "post";
	}
	
	@ResponseBody
	@RequestMapping(value = "/{slug}", method = RequestMethod.GET)
	public String slugs(@PathVariable String slug){
		return "hola "+slug;
	}
	
	@ResponseBody
	@RequestMapping(value = "/{year}/{month}/{day}/{slug}/comment", method = RequestMethod.GET)
	public String comment(@PathVariable String year, @PathVariable String month, 
			@PathVariable String day, @PathVariable String slug){
		return "hola "+year+"/"+month+"/"+day+"/"+slug;
	}
	
	@ResponseBody
	@RequestMapping(value = "/person/{name}/{surName}", method = RequestMethod.GET)
	public String findPerson(Person person){
		return "person: "+person.getName()+" "+person.getSurName();
	}
	
	
	@ResponseBody
	@RequestMapping(value="/jsonc/{name}", consumes=MediaType.APPLICATION_JSON_VALUE)
	public Person searchPerson(@RequestBody Person person) {
	   return person;
	}
	
	
	
	@RequestMapping(value = "/jsonp/{name}/{surname}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Person getPerson(@PathVariable String name) {
		Person p = new Person();
		p.setName(name);
		return p;
	}
	
	@RequestMapping(value = "/jsonp/{name}/{surname}", method = RequestMethod.GET, headers=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Person getPerson2(@PathVariable String name) {
		Person p = new Person();
		p.setName(name);
		return p;
	}
}
