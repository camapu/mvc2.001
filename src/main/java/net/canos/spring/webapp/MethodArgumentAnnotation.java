package net.canos.spring.webapp;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/method-annotation")
public class MethodArgumentAnnotation {

	
	@ResponseBody
	@RequestMapping(value = "/path/{variable}", method = RequestMethod.GET)
	public String path(@PathVariable String variable){
		return "variable:"+variable;
	}
	
	@ResponseBody
	@RequestMapping(value = "/param", method = RequestMethod.GET)
	public String param(@RequestParam(required=false,defaultValue="") String name){
		return "name:"+name;
	}
	
	@ResponseBody
	@RequestMapping(value = "/matrix", method = RequestMethod.GET)
	public String matrix(@MatrixVariable Map<String, String> matrix){
		return "name:"+matrix.toString();
	}
	/* MatrixVariable requires removeSemicolonContent from HandlerMapping
	 * in a @WebMvcConfigurationSupport Bean
	 * 
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
	    final RequestMappingHandlerMapping requestMappingHandlerMapping = super.requestMappingHandlerMapping();
	    requestMappingHandlerMapping.setRemoveSemicolonContent(false); // <<< this
	    return requestMappingHandlerMapping;
	}
	 * */

	@ResponseBody
	@RequestMapping(value = "/request-header", method = RequestMethod.GET)
	public String request(@RequestHeader(value="User-Agent") String ua){
		return "UserAgent:"+ua.toString();
	}
	
	/**
	 * Recibir objetos json
	 */
	@ResponseBody
	@RequestMapping(value = "/request-body", method = RequestMethod.GET)
	public String body(@RequestBody(required=false) Person person){
		return "Body:"+person.getName();
	}
	
	/** subida de ficheros */
	@ResponseBody
	@RequestMapping(value = "/request-part", method = RequestMethod.GET)
	public String part(@RequestPart(required=false) Person person){
		return "Body:"+person.getName();
	}
	
}
