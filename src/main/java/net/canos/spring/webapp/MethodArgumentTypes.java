package net.canos.spring.webapp;

import java.io.IOException;
import java.io.Reader;
import java.security.Principal;
import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/method-argument")
public class MethodArgumentTypes {

	@ResponseBody
	@RequestMapping(value = "/session", method = RequestMethod.GET)
	public String session(HttpSession session){
		return "Session:"+session.getId();
	}
	
	@ResponseBody
	@RequestMapping(value = "/request", method = RequestMethod.GET)
	public String request(HttpServletRequest request){
		return "request URI:"+request.getRequestURI();
	}
	
	@ResponseBody
	@RequestMapping(value = "/request2", method = RequestMethod.GET)
	public String request2(ServletRequest request){
		return "request remote addr:"+request.getRemoteAddr();
	}
	
	@ResponseBody
	@RequestMapping(value = "/request3", method = RequestMethod.GET)
	public String request3(WebRequest request){
		return "request sesion id:"+request.getSessionId();
	}
	
	@ResponseBody
	@RequestMapping(value = "/request4", method = RequestMethod.GET)
	public String request4(NativeWebRequest request){
		return "request sesion id:"+request.getSessionId();
	}
	
	@ResponseBody
	@RequestMapping(value = "/multipart", method = RequestMethod.GET)
	public String multipart(MultipartHttpServletRequest request){
		MultipartFile file = request.getFile("fichero");
		return "contentType:"+file.getContentType();
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/locale", method = RequestMethod.GET)
	public String locale(Locale locale){
		return "locale:"+locale;
	}
	
	@ResponseBody
	@RequestMapping(value = "/method", method = RequestMethod.GET)
	public String method(HttpMethod method){
		return "method:"+method;
	}
	
	@ResponseBody
	@RequestMapping(value = "/stream", method = RequestMethod.GET)
	public String method(Reader stream) throws IOException{
		return "reader:"+stream.toString();
	}
	
	
	
	@ResponseBody
	@RequestMapping(value = "/redirectAttributes", method = RequestMethod.GET)
	 public String handle(Person account, BindingResult result, RedirectAttributes redirectAttrs) {
	   if (result.hasErrors()) {
	     return "accounts/new";
	   }
	   // Save account ...
	   redirectAttrs.addAttribute("id", account.getName()).addFlashAttribute("message", "Account created!");
	   return "redirect:/accounts/{id}";
	 }
	
	 @ResponseBody
	 @RequestMapping(value = "/principal", method = RequestMethod.GET)
	 public String principal(Principal principal, HttpServletRequest request) {
		Principal p = request.getUserPrincipal();
	   return principal.getName()+" == "+p.getName();
	 }
	 
	 
	 /**
	  * Información relativa a HTTP
	  */
	 @ResponseBody
	 @RequestMapping(value = "/entity", method = RequestMethod.GET)
	 public String entity(HttpEntity<?> entity) {
		 return entity.toString();
	 }
}
